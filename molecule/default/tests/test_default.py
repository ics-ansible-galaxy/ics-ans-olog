import os
import testinfra.utils.ansible_runner
import pytest
import time


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture(scope="module", autouse=True)
def healthcheck(host):
    for i in range(200):
        with host.sudo():
            cmd = host.run("docker exec olog wildfly/bin/jboss-cli.sh --connect 'deployment-info --name=olog-service*.war'")
            if cmd.stdout.splitlines()[1].split()[4] == "OK":
                return
        time.sleep(3)
    raise RuntimeError('Timed out waiting for application to start.')


def test_application_container(host):
    with host.sudo():
        olog = host.docker("olog")
        assert olog.is_running


def test_application_ui(host):
    cmd = host.run("curl --insecure --fail https://ics-ans-olog-default/")
    assert cmd.rc == 0
    assert "title>Login Form</title>" in cmd.stdout


def test_database_container(host):
    with host.sudo():
        database = host.docker("olog-database")
        assert database.is_running
